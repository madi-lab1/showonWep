import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";

export const useUserStore = defineStore("user", () => {
  const sum = ref(0);
  const amount = ref(0);

  const JWB = () => {
    sum.value = sum.value + 9505.0;
    items.push({
      name: "Johnnie Walker Blue Label (750 ml)",
      price: "฿9,505.00",
    });
    amount.value = amount.value + 1;
  };

  const JWG = () => {
    sum.value = sum.value + 2159.0;
    items.push({
      name: "Johnnie Walker Green Label (750 ml)",
      price: "฿2159.00",
    });
    amount.value = amount.value + 1;
  };

  const JWBK = () => {
    sum.value = sum.value + 1300.0;
    items.push({
      name: "Johnnie Walker Black Label (750 ml)",
      price: "฿1300.00",
    });
    amount.value = amount.value + 1;
  };

  const JWR = () => {
    sum.value = sum.value + 679.0;
    items.push({
      name: "Johnnie Walker Red Label (700 ml)",
      price: "฿679.00",
    });
    amount.value = amount.value + 1;
  };

  const REC = () => {
    sum.value = sum.value + 845.0;
    items.push({
      name: "Hankey Bannister REGENCY (700 ml)",
      price: "฿845.00",
    });
    amount.value = amount.value + 1;
  };

  const JDD = () => {
    sum.value = sum.value + 1150.0;
    items.push({
      name: "Jack Daniel’s No.7 (1000 ml)",
      price: "฿1150.00",
    });
    amount.value = amount.value + 1;
  };
  const CL = () => {
    sum.value = sum.value + 27.0;
    items.push({
      name: "Cola",
      price: "฿27.00",
    });
    amount.value = amount.value + 1;
  };

  const SW = () => {
    sum.value = sum.value + 15.0;
    items.push({
      name: "Schweppes",
      price: "฿15.00",
    });
    amount.value = amount.value + 1;
  };

  const SD = () => {
    sum.value = sum.value + 10.0;
    items.push({
      name: "Soda",
      price: "฿10.00",
    });
    amount.value = amount.value + 1;
  };

  const OJ = () => {
    sum.value = sum.value + 80.0;
    items.push({
      name: "Orange Juice",
      price: "฿80.00",
    });
    amount.value = amount.value + 1;
  };

  const BW = () => {
    sum.value = sum.value + 150.0;
    items.push({
      name: "Blue Hawaii",
      price: "฿150.00",
    });
    amount.value = amount.value + 1;
  };

  const WT = () => {
    sum.value = sum.value + 15.0;
    items.push({
      name: "Water",
      price: "฿15.00",
    });
    amount.value = amount.value + 1;
  };

  const FF = () => {
    sum.value = sum.value + 50.0;
    items.push({
      name: "French Fries",
      price: "฿50.00",
    });
    amount.value = amount.value + 1;
  };

  const NG = () => {
    sum.value = sum.value + 80.0;
    items.push({
      name: "Nuggets",
      price: "฿80.00",
    });
    amount.value = amount.value + 1;
  };

  const POPC = () => {
    sum.value = sum.value + 40.0;
    items.push({
      name: "Popcorn",
      price: "฿40.00",
    });
    amount.value = amount.value + 1;
  };

  const CHIP = () => {
    sum.value = sum.value + 25.0;
    items.push({
      name: "Chips",
      price: "฿25.00",
    });
    amount.value = amount.value + 1;
  };

  const FC = () => {
    sum.value = sum.value + 150.0;
    items.push({
      name: "Fried Chicken",
      price: "฿150.00",
    });
    amount.value = amount.value + 1;
  };

  const HD = () => {
    sum.value = sum.value + 30.0;
    items.push({
      name: "Hot Dog",
      price: "฿30.00",
    });
    amount.value = amount.value + 1;
  };
  //delete

  const DJWB = () => {
    items.splice();
    if (amount.value > 0) {
      sum.value = sum.value - 9505.0;
      amount.value = amount.value - 1;
    }
  };
  const items: { name: string; price: string }[] = [];

  return {
    items,
    JWB,
    JWG,
    JWBK,
    JWR,
    REC,
    JDD,
    CL,
    SW,
    SD,
    OJ,
    BW,
    WT,
    sum,
    amount,
    DJWB,
    FF,
    NG,
    POPC,
    CHIP,
    FC,
    HD,
  };
});
